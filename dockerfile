FROM alpine:3.10

MAINTAINER  Yasser ABDRABOU <yasser.abdrabou@altersis.com>

#We will not use it for now
EXPOSE 4445

#Defining environment variables
ARG JMETER_VERSION="5.1.1"
ENV JMETER_HOME /opt/apache-jmeter-${JMETER_VERSION}
ENV	JMETER_BIN	${JMETER_HOME}/bin
ENV	JMETER_DOWNLOAD_URL https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz
ENV JMETER_PLUGINS_MANAGER_VERSION 1.3
ENV CMDRUNNER_VERSION 2.2

#defining time zone 
RUN apk update
RUN apk upgrade
RUN apk add ca-certificates && update-ca-certificates
# Change TimeZone
RUN apk add --update tzdata
ENV TZ=Europe/Zurich
# Clean APK cache
RUN rm -rf /var/cache/apk/\*

#Install apache-jmeter 5.1.1
RUN  apk update \
&& apk upgrade \
&& apk add ca-certificates \
&& update-ca-certificates \
&& apk add tar \
&& apk add --update openjdk8-jre tzdata curl unzip bash \
&& apk add --no-cache nss \
&& rm -rf /var/cache/apk/\* \
&& mkdir -p /tmp/dependencies \
&& curl -L --silent ${JMETER_DOWNLOAD_URL} >  /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz \
&& mkdir -p /opt  \
&& tar -xzf /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz -C /opt \
&& rm -rf /tmp/dependencies/\*


ENV PATH $PATH:$JMETER_BIN
# Install the jmeter plugin manager and we add jpgc web driver
RUN cd /tmp/ \
&& curl --location --silent --show-error --output ${JMETER_HOME}/lib/ext/jmeter-plugins-manager-${JMETER_PLUGINS_MANAGER_VERSION}.jar http://search.maven.org/remotecontent?filepath=kg/apc/jmeter-plugins-manager/${JMETER_PLUGINS_MANAGER_VERSION}/jmeter-plugins-manager-${JMETER_PLUGINS_MANAGER_VERSION}.jar \
&& curl --location --silent --show-error --output ${JMETER_HOME}/lib/cmdrunner-${CMDRUNNER_VERSION}.jar http://search.maven.org/remotecontent?filepath=kg/apc/cmdrunner/${CMDRUNNER_VERSION}/cmdrunner-${CMDRUNNER_VERSION}.jar \
&& java -cp ${JMETER_HOME}/lib/ext/jmeter-plugins-manager-${JMETER_PLUGINS_MANAGER_VERSION}.jar org.jmeterplugins.repository.PluginManagerCMDInstaller \
&& PluginsManagerCMD.sh install \
jpgc-webdriver=3.1, \
jmeter-plugins-webdriver-1.3.jar, \

ENV PATH $PATH:$JMETER_BIN

#From the container point of view, the script is into folder “/mnt/jmeter” (see previous export command). 
#In reality, the jmx script is external to container and provided via  the Docker volume.
RUN mkdir /mnt/project


# the entry point bash script used by the container as an entrypoint for JMeter arguments.
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

#define the work direction into container as apache-jmeter/bin folder
WORKDIR	${JMETER_HOME}

ENTRYPOINT ["/entrypoint.sh"]
