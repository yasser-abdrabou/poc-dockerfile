# Poc-Dockerfile
# Prerequiste:
# Installing Docker 
Docker must be installed as a service on the host machine that executes the containers.

# How to Use the Docker Container with JMeter-Webdriver ?

Passing JMeter arguments with the “docker run” command which jmx script must be executed, script parameters,... from a docker volume in local machine
mounted into docker container in specific file " /mnt/project"
Then, fetching the result file (e.g. jtl and log file) using the same shared folder on the test machine called Docker volumethat can be used to save result files after the container execution ends.

# Easy Workflow :
 1 	let’s build an image from Dockerfile in the current repository with this command: 
 >> "docker build -t jmeter ." you can downloaded
 2 Or you can pull it from this docker-hub repository using this command : docker pull yasserabd/jmeter5.1.1-webdriver:1.2.0 
 
 # How to run the docker container ? 
 
 First we need to create a docker volume to exchange files from local with the container. Use the command
 >> docker volume create <volume name> e.g jmeter-project
 Now we can retrieve where the volume is mapped on the test machine.Use this command 
 >>docker volume inspect jmeter-project

# Now run the test script by running the docker container by specify the volume directly with the container execution command line via arguments ! using this command :

>> docker run --volume “${volume_path}”:${jmeter_path} ${image_name} 

# JMeter result files are available under the temporary folder in the <volume_path> and can be used as a normal JMeter result file.

