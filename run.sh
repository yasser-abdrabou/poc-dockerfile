#!/bin/sh

#Run JMeter Docker image with options

export timestamp=$(date +%Y%m%d_%H%M%S) && \
export volume_path=/var/lib/docker/volumes/testing/_data/test && \
export jmeter_path=/test && \
docker run \
  --volume “${volume_path}”:${jmeter_path} \
  jmeter \
  -n <any sequence of jmeter args> \
  -t ${jmeter_path}/test-plan.jmx \
  -l ${jmeter_path}/tmp/result_${timestamp}.jtl \
  -j ${jmeter_path}/tmp/jmeter_${timestamp}.log 